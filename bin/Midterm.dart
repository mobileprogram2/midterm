import 'dart:io';
import 'dart:math';

void main() {
  stdout.write("Input String");
  String s = stdin.readLineSync()!;
  print(Tokenizing(s));

  stdout.write("Infix :");
  List token = Tokenizing(s);
  print(infixToPostfix(token)); 

  stdout.write("Postfix");
  List postfix = infixToPostfix(token);
  
  stdout.write("Evalute Postfix :");
  print(evalutePosfix(postfix));
  List evaliate =evalutePosfix(postfix);
  print(evaliate);
}

List Tokenizing(String s) {
  List<String> list = [];
  String t = " ";
  for (int i = 0; i < s.length; i++) {
    if (s[i] == " " && t.isNotEmpty) {
      list.add(t);
      t = "";
    } else {
      t = t + s[i];
    }
  }
  if (!s[s.length - 1].contains(" ")) {
    list.add(t);
  }
  return list;
}

List infixToPostfix(List token) {
  List<String> operatorlist = [];
  List<String> postlist = [];
  List<String> operationlist = ["+", "-", "^", "*", "/"];
  Map<String, int> valuesOfOperator = {
    "+": 1,
    "-": 1,
    "^": 3,
    "*": 2,
    "/": 2,
    "(": 4,
    ")": 4
  };
  for (int i = 0; i < token.length; i++) {
    if (int.tryParse(token[i]) != null) {
      postlist.add(token[i]);
    }
    if (operationlist.contains(token[i])) {
      while (operatorlist.isNotEmpty &&
          operatorlist.last != "(" &&
          valuesOfOperator[token[i]]! < valuesOfOperator[token.last]!) {
        postlist.add(operatorlist.removeLast());
      }
      operatorlist.add(token[i]);
    }
    if (token[i] == "(") {
      operatorlist.add(token[i]);
    }
    if(token[i] == ")"){
      while(operatorlist.last !="("){
        postlist.add(operatorlist.removeLast());
      }
      operatorlist.removeLast();
    }
  }
  while (operatorlist.isNotEmpty){
    postlist.add(operatorlist.removeLast());
  }

  return postlist;
}

List evalutePosfix (List postlist){
  List<num> sum = [];
  var right, left, total;
  for(var p in postlist){
    if(!"+-*/^".contains(p)){
      sum.add(double.parse(p));
    }else{
      right = sum[sum.length-1];
      sum.removeLast();
      left = sum[sum.length-1];
      sum.removeLast();
      if(p == "+"){
        total = left + right;
        sum.add(total);
      }
      else if(p == "-"){
        total = left + right;
        sum.add(total);
      }
      else if(p == "*"){
      total = left * right;
        sum.add(total);
      }
      else if(p == "/"){
        total = left / right;
        sum.add(total);
      }
      else if(p == "^"){
        total = pow(left , right);
        sum.add(total);
      }
    }
  }
  

  
  return sum;
}


